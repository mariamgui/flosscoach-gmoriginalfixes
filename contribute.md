# Contributing to FLOSScoach project

Hi there! We're thrilled that you'd like to contribute to this project. Your help is essential for keeping it great.

These are some of the options if you want to support us:
- [File an issue](#file-an-issue)
- [Find an issue to work on](#find-an-issue-to-work-on)
- [Help us with the project documentation](#make-a-documentation-contribution)
- [Help us coding: adding features or fixing bugs](#help-us-with-code)

If you want to report a problem or discuss an idea about the project, you can also [file an issue (bug report or new feature request)](#file-an-issue).

# File an issue
- Did you find any problem while using FLOSScoach?
- Do you have any great idea of new feature that would make FLOSScoach nicer? 

Don't keep it to yourself. Please let us know! Reporting issues and providing feature requests are one of the things that makes Open Source amazing. 
Please help us by creating a new issue report in our GitLab repository (either bug report or feature request). 

If you want to file an issue:

- Navigate to the [issues' list](https://gitlab.com/mariamgui/flosscoach-gmoriginalfixes/-/issues)
- Create a issue clicking on the "New issue" button
- Fill the information such as title, description. Provide as much details as you can.
- Click on the "Create issue" button 

Remember to stay tuned in your issue to provide more details as requested by other members in the follow up comments.

# Find an issue to work on

If you want to help us fixing an issue that have been already reported, you should take a look at our [list of issues](https://gitlab.com/mariamgui/flosscoach-gmoriginalfixes/-/issues):

- Navigate to the [issues' list](https://gitlab.com/mariamgui/flosscoach-gmoriginalfixes/-/issues)
- Just starting out? If you a novice to this projects, use this [search](https://gitlab.com/mariamgui/flosscoach-gmoriginalfixes/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=good%20for%20newcomers) to find issues marked as "good for newcomers".

# Make a documentation contribution

If you want to help us fixing typos or improving the documentation, you should take a look at our markdown files (e.g., README.md, contribute.md):

- Navigate to the [file's search](https://gitlab.com/mariamgui/flosscoach-gmoriginalfixes/-/find_file/master)
- Write down ".md" in the *Find by path* field to find the markdown files' list.
- Click on the file you want to contribute to.
- Click on the *Edit* button (blue button). You will be requested to create a *Fork* (copy) of the project.
- Edit the file.
- Submit your merge request by following the [GitLab tutorial](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

# Help us with code

If you're willing to contribute with code to  **FLOSScoach** these are the simple setps you must follow to **get your local machine ready for development**. Tough easy and simple, these steps are estimated to take up to 30min.

- We're focusing here on the **Linux operational system** (Ubuntu, Mint and other distributions). Steps may vary in case of MacOS or Windows. 

## Step 1 - Rails Dependencies dependencies 
There are some core dependencies that must be installed for the framework *Ruby on Rails* in which *FLOSScoach* is built on top of.

### 1.1 Adding Node.js and Yarn repositories
Running the following command will add Node.js and Yarn repositories to your machine:
```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev nodejs yarn
```

### 1.2 Installing Ruby through rbenv
rbenv (Ruby virtual environment) is a good, stable and simple way of installing the Ruby programming language interpreter. Here's how to do it:
'''
```bash
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL

rbenv install 2.3
rbenv global 2.3
ruby -v
```
Please note that we're installing Ruby version 2.3 

### 1.3 Installing Bundler 
Bundler is the last dependencie. SImply execute a
```bash
gem install bundler
```
and it's all done.

## Step 2 - Configuring Git

Version control is quintessential while contributing to open source projects and working with code in general. Here's how to set your *Git* configurations:
```bash
git config --global color.ui true
git config --global user.name "Insert your name here"
git config --global user.email "yourEmailAdress@here.com"
```
You might want to use the same email as the one used for yout Github/lab account.

## Step 3 - Installing Node.js and Rails
```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```
```bash
gem install rails -v 5.2.1
```



## Step 4 - Database: Setting up Sqlite3 and Postgre SQL
Installing Postgre SQL can be done with the following commands:
```bash
sudo sh -c "echo 'deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main' > /etc/apt/sources.list.d/pgdg.list"
wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-common
sudo apt-get install postgresql-9.5 libpq-dev
```
Lastly, you need to set an user and password so then *FLOSScoach* database setup will be albe to explore and modify the development database
```bash
# python is the username in the development set up.
sudo -u postgres createuser python -s
```
It's important to jnow that the default development password is **python** and you must set it just like this
```bash
# Setting password for the user named python:
sudo -u postgres psql
\password python
# And you will be prompted to type the new password
```
In case you want different user/passoword just be sure to match these with 
the present in the *config/database.yml* file 

## Step 5 - Cloning the FLOSScoach repository
Now that everything is ready, it's time to clone the repository of the project to run your local development server and start contributing
```bash
cd ~
# Cloning the development branchcan be done like so: git clone -b <branch> <remote_repo>
git clone -b development https://gitlab.com/flosscoach/flosscoach.git
#Navigate to the newly created repository clone:
cd flosscoach
```
Some final small adjustments:
```bash
rake db:migrate RAILS_ENV=development
rails server -p 8000
```
Finally done!! See your local server running on [localhost:8000](http://localhost:8000)

## Step 6 - Merging your contributions
After you have finished writting some code these are the steps for adding your contribution to FLOSScoach:

### Step 6.1 Add changes to GitLab
```bash
#cd to the project directory
cd flossccoach
#Add your local changes (. will add all changes done)
git add .
#Create a new commit
git commit -m "Insert commit message here"
#And then send it to the Gitlab repository 
git push origin development
#You will be prompted to insert your Gitlab login credentials then
```
### Step 6.2 Merging changes
To merge the changes you've done on development to master branch you can follow the excelent instructions from [Gitlab official docs](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
It's quite simple to be done.

