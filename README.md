![logo](https://gitlab.com/flosscoach/flosscoach/raw/master/app/assets/images/flosscoach-logo.png) 
# FLOSScoach
We are a community aimed to help newcomers get into *Free/Libre Open Source Software* (FLOSS). FLOSScoach helps by providing a way to organize the information, 
benefiting newcomers to avoid or overcome the most common barriers they face according to the research literature. The tool enables the inclusion of information
about existing projects in a standardized way, making it easier for newcomer to get used to the contribution flow.

FLOSSCoach portal has individual project pages, foruns and messages where you can exchange relevant information about OSS with newcomers so they can start contributing.

## Supporting the project 

FLOSScoach is built by a community. We are happy to welcome new contributors! 

If you're interested in contributing to our projects and not sure how to start, you can start by fixing typos in the documentation, reporting an issue, or even requesting a new feature.

Please check out our [contributing docs](contribute.md) to get started. You will find more information on how to file an issue, find a task to work on, and setting the environment to work with a code contribution.

### Deployment process
Want to understand the deployment process? Check our (Deploying.md) 

### Build status
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/flosscoach/flosscoach.svg)


## Authors and contacts
FLOSScoach was idealized as part of a research. The main contact points are:

- Igor Steinmacher @igorsteinmacher (igor.steinmacher [a] nau [] edu)
- Igor Wiese @igorwiese (igor [a] utfpr [] edu [] br)
- Felipe Fronchetti @fronchetti (fronchetti [a] usp [] br)
- Marco Gerosa (marco.gerosa [a] nau [] edu))

Feel free to contact us, or to mention one of us in the issue tracker

Other developers, students, and colleagues also contributed to the project. We are thankful to all of them (https://gitlab.com/flosscoach/flosscoach/-/graphs/master)





